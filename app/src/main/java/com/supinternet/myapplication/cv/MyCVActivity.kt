package com.supinternet.myapplication.cv

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.supinternet.myapplication.BaseActivity
import com.supinternet.myapplication.R
import com.supinternet.myapplication.R.id.cv_list
import com.supinternet.myapplication.cv.model.*
import kotlinx.android.synthetic.main.cv_activity.*
import kotlinx.android.synthetic.main.cv_activity.view.*
import kotlinx.android.synthetic.main.cv_item_education.view.*
import kotlinx.android.synthetic.main.cv_item_skill.view.*
import kotlinx.android.synthetic.main.cv_item_title.view.*
import kotlinx.android.synthetic.main.cv_item_work_experience.view.*
import java.lang.Exception

class MyCVActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cv_activity)

        cv_list.layoutManager = LinearLayoutManager(this)
        cv_list.adapter = CVAdapter(generateItems())
    }

    private fun generateItems(): List<CVItem> {
        return listOf(
                Title("Mes compétences"),
                Skill("Android", 80),
                Skill("Kotlin", 60),
                Skill("Javascript", 50),
                Skill("HTML", 90),
                Skill("CSS", 30),
                Title("Mes diplômes"),
                Education("DUT Informatique", "Université de Valence", "Valence", 2018),
                Education("Bac S Spécialité Mathématiques", "Lycée Mermoz", "Paris", 2017),
                Title("Mes expériences"),
                WorkExperience("Développeur", "SUP'Internet", "Paris", "2018 - 2019"),
                WorkExperience("Développeur", "Le Monde", "Lyon", "2012 - 2018"),
                WorkExperience("Rédacteur", "Humanoid", "Paris", "2010 - 2012")
        )
    }
}

private class CVAdapter(val items: List<CVItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEM_TYPE_TITLE = 0
    private val ITEM_TYPE_SKILL = 1
    private val ITEM_TYPE_EDUCATION = 2
    private val ITEM_TYPE_WORK_EXPERIENCE = 3

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when(viewType) {
            ITEM_TYPE_TITLE -> TitleViewHolder(inflater.inflate(R.layout.cv_item_title, parent, false))
            ITEM_TYPE_SKILL -> SkillViewHolder(inflater.inflate(R.layout.cv_item_skill, parent, false))
            ITEM_TYPE_EDUCATION -> EducationViewHolder(inflater.inflate(R.layout.cv_item_education, parent, false))
            ITEM_TYPE_WORK_EXPERIENCE -> WorkExperienceViewHolder(inflater.inflate(R.layout.cv_item_work_experience, parent, false))
            else -> throw Exception("Unknown view type")
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is TitleViewHolder ->
                holder.bindTitle(items[position] as Title)
            is SkillViewHolder ->
                holder.bindSkill(items[position] as Skill)
            is EducationViewHolder ->
                holder.bindEducation(items[position] as Education, isLastItem(position))
            is WorkExperienceViewHolder ->
                holder.bindWorkExperience(items[position] as WorkExperience, isLastItem(position))
        }
    }

    private fun isLastItem(position: Int): Boolean {
        if (position == itemCount - 1) {
            return true
        } else if (items[position + 1].javaClass != items[position].javaClass) {
            return true
        }

        return false
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is Title -> ITEM_TYPE_TITLE
            is Skill -> ITEM_TYPE_SKILL
            is Education -> ITEM_TYPE_EDUCATION
            is WorkExperience -> ITEM_TYPE_WORK_EXPERIENCE
            else -> throw Exception("Unknown view Type")
        }
    }
}

private class TitleViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    private val title : AppCompatTextView = v.cv_title

    fun bindTitle(item: Title) {
        title.text = item.name
    }

}

private class SkillViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    private val name : AppCompatTextView = v.cv_skill_name
    private val progress : ProgressBar = v.cv_skill_progress

    fun bindSkill(item: Skill) {
        name.text = item.name
        progress.progress = item.percent
    }

}

private class EducationViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    private val name : AppCompatTextView = v.cv_education_name
    private val school : AppCompatTextView = v.cv_education_school
    private val details : AppCompatTextView = v.cv_education_details
    private val line : View = v.cv_education_line

    fun bindEducation(item: Education, isLastItem: Boolean) {
        name.text = item.name
        school.text = item.school
        details.text = details.context.getString(R.string.cv_education_details, item.location, item.year)
        line.visibility = if (isLastItem) View.GONE else View.VISIBLE
    }

}

private class WorkExperienceViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    private val name : AppCompatTextView = v.cv_work_experience_name
    private val company : AppCompatTextView = v.cv_work_experience_company
    private val details : AppCompatTextView = v.cv_work_experience_details
    private val line : View = v.cv_work_experience_line

    fun bindWorkExperience(item: WorkExperience, isLastItem: Boolean) {
        name.text = item.jobName
        company.text = item.company
        details.text = details.context.getString(R.string.cv_work_experience_details, item.location, item.duration)
        line.visibility = if (isLastItem) View.GONE else View.VISIBLE
    }

}