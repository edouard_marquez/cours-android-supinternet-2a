package com.supinternet.myapplication.cv.model

data class Skill(
    var name: String,
    var percent: Int
) : CVItem()

data class Education(
    var name: String,
    var school: String,
    var location: String,
    var year: Int
) : CVItem()

data class WorkExperience(
        var jobName: String,
    var company: String,
    var location: String,
    var duration: String
) : CVItem()

data class Title(
        var name: String
) : CVItem()

open class CVItem