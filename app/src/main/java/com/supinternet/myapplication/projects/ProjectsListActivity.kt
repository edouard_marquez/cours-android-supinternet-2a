package com.supinternet.myapplication.projects

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.supinternet.myapplication.BaseActivity
import com.supinternet.myapplication.R
import com.supinternet.myapplication.projects.model.Project
import kotlinx.android.synthetic.main.projects_list_activity.*
import kotlinx.android.synthetic.main.projects_list_item.view.*

class ProjectsListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.projects_list_activity)

        val projets = listOf(
                Project(
                        name = "Douche Simulator",
                        description = "Le seul et unique simulateur de douche pour Android.\nDisponible sur le Google Play, et il est entièrement gratuit !",
                        category = "Android",
                        categoryColor = Color.RED,
                        websiteLink = "https://play.google.com/store/apps/details?id=com.antrodev.douchesimulator",
                        pictureLink = "https://lh3.ggpht.com/-QI-VL8p35AXV_14jULopHZko0OfwGFEmIbUBdkIvbkTZho3DqmjQyYv7WVdL7l9yaU=w2560-h2520"),

                Project(
                        name = "BlaBlaSims",
                        description = "Le seul et unique service de covoiturage entre Sims. Une manière écologique de partager ses frais",
                        category = "Node JS",
                        categoryColor = Color.GREEN,
                        websiteLink = "https://www.blablacar.fr",
                        pictureLink = "https://thegirlwhogames.files.wordpress.com/2014/09/wpid-wp-1410959539621.jpeg"),

                Project(
                        name = "Hot Dog Not Hot Dog",
                        description = "Bresaola kielbasa tenderloin chicken shankle tri-tip picanha meatloaf. Pork sausage turkey bacon, jowl ground round tail tenderloin alcatra porchetta beef cupim brisket sirloin doner.",
                        category = "LOL",
                        categoryColor = Color.BLUE,
                        websiteLink = "https://play.google.com/store/apps/details?id=com.codylab.seefood",
                        pictureLink = "https://lh3.googleusercontent.com/JGT4B8M8VJLo91Gc1J9cR9Qni14r40UEVfMwR9sJ-HC-Gjk1I4YPNIdx4-8H-EmjpIk=w2560-h2520")
        )

        projects_list.layoutManager = LinearLayoutManager(this)
        projects_list.adapter = ProjectsListAdapter(projets)
    }
}

private class ProjectsListAdapter(val projects : List<Project>) : RecyclerView.Adapter<ProjectViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, itemViewType: Int): ProjectViewHolder {
        return ProjectViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.projects_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
        holder.bindProject(projects[position])
    }

    override fun getItemCount(): Int {
        return projects.size
    }

}

private class ProjectViewHolder(v : View) : RecyclerView.ViewHolder(v) {

    private val picture : AppCompatImageView = v.project_picture
    private val title : AppCompatTextView = v.project_name
    private val category : AppCompatTextView = v.project_category
    private val description : AppCompatTextView = v.project_description

    fun bindProject(project: Project) {
        title.text = project.name
        category.text = project.category
        DrawableCompat.setTintList(category.background, ColorStateList.valueOf(project.categoryColor))

        description.text = project.description

        Picasso.get().load(project.pictureLink).into(picture)
    }

}