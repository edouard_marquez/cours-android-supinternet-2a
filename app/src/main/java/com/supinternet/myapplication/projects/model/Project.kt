package com.supinternet.myapplication.projects.model

data class Project(
    val name: String,
    val description: String,
    val category: String,
    val categoryColor: Int,
    val websiteLink: String,
    val pictureLink: String
)
